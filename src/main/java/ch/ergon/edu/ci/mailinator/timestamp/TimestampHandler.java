package ch.ergon.edu.ci.mailinator.timestamp;

import ch.ergon.edu.ci.mailinator.domain.Letter;

public final class TimestampHandler {

    private TimestampHandler() {

    }


    public static void startProcessing(Letter letter) {
        letter.setStartProcessingTimestamp(System.nanoTime());
    }

    public static void endProcessing(Letter letter) {
        letter.setEndProcessingTimestamp(System.nanoTime());
    }
}
