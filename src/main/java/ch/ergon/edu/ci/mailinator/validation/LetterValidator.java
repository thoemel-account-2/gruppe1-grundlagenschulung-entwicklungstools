package ch.ergon.edu.ci.mailinator.validation;

import ch.ergon.edu.ci.mailinator.domain.Letter;

import java.util.Arrays;
import java.util.List;

public class LetterValidator {
    private static final int TIEF = 1000;
    private static final int HOCH = 9999;

    private static final List<String> VALID_CITIES = Arrays.asList(
            "Zürich", "Genf", "Basel", "Bern", "Lausanne", "Winterthur",
            "Luzern", "St. Gallen", "Lugano", "Biel/Bienne", "Thun", "Köniz",
            "La Chaux-de-Fonds", "Freiburg", "Schaffhausen", "Chur", "Vernier",
            "Neuenburg", "Uster", "Sitten");

    public LetterValidator() {
        System.out.println("Valid cities: " + VALID_CITIES);
    }

    public boolean isValid(Letter letter) {
        return (isPlzFormatCorrect(letter.getPlz())
                && isContentNotEmpty(letter.getContent())
                && isValidCity(letter.getCity()));

    }

    boolean isPlzFormatCorrect(int plz) {
        return plz >= TIEF && plz <= HOCH;
    }

    boolean isContentNotEmpty(String content) {
        return content != null && !content.equals("");
    }

    boolean isValidCity(String city) {
        if (city != null) {
            for (String validCity : VALID_CITIES) {
                if (city.equals(validCity)) {
                    return true;
                }
            }
        }
        return false;
    }

}
