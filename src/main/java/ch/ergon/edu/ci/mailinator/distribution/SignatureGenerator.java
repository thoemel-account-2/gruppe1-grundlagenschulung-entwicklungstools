package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.Letter;

import java.io.*;
import java.nio.charset.StandardCharsets;

public final class SignatureGenerator {
    private SignatureGenerator() {

    }

    public static void generateSignature(Letter letter) {
        try {
            File file = new File("recources/Signature.txt");
            InputStreamReader streamReader = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while (reader.ready()) {
                stringBuffer.append(reader.readLine() + "\n");
            }
            letter.setSignature(stringBuffer.toString());
            streamReader.close();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
