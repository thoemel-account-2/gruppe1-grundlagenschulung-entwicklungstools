package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.AddressEntry;
import ch.ergon.edu.ci.mailinator.domain.Letter;

public final class AddressGenerator {
    private static final String SPACE = " ";

    private AddressGenerator() {

    }

    public static void insertAddress(Letter letter, AddressEntry addressEntry) {
        letter.setName(addressEntry.getFirstName() + SPACE + addressEntry.getLastName());
        letter.setStreet(addressEntry.getStreetName() + SPACE + addressEntry.getHouseNumber());
        letter.setCity(addressEntry.getCity());
        letter.setPlz(Integer.parseInt(addressEntry.getPlz()));
    }
}
