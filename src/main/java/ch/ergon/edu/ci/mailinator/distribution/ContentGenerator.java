package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.Letter;

import java.util.Random;

public class ContentGenerator {
    private static final int MAX_CHAR = 122;
    private static final int MIN_CHAR = 97;
    private static final int CONTENT_LENGTH = 128;

    public void insertContent(Letter letter) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < CONTENT_LENGTH; i++) {
           stringBuffer.append(getRandomChar());
        }
        letter.setContent(stringBuffer.toString());
    }

    private char getRandomChar() {
        Random random = new Random();
        return (char) (random.nextInt((MAX_CHAR - MIN_CHAR) + 1) + MIN_CHAR);
    }
}
