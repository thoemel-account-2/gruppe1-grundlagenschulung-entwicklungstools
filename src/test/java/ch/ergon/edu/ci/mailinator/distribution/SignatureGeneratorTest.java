package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.junit.Assert;
import org.junit.Test;

public class SignatureGeneratorTest {

    @Test
    public void notNull() {
        Letter letter = new Letter();
        SignatureGenerator.generateSignature(letter);
        Assert.assertNotNull(letter.getSignature());
    }

    @Test
    public void assertString() {
        Letter letter = new Letter();
        SignatureGenerator.generateSignature(letter);
        Assert.assertEquals("Freundliche Grüsse\nGit Cheat Sheet\n", letter.getSignature());
    }
}
