package ch.ergon.edu.ci.mailinator.timestamp;

import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TimestampHandlerTest {
    private static final long SEC_IN_NANOSEC = 1000000000;
    /**
     * Unit Test zu Aufgabe 11
     */
    @Test
    public void biggerThan0Start() {
        Letter letter = new Letter();
        TimestampHandler.startProcessing(letter);
        assertTrue(letter.getStartProcessingTimestamp() > 0);
    }

    /**
     * Unit Test zu Aufgabe 11
     */
    @Test
    public void biggerThanPreSetVariableStart() {
        Letter letter = new Letter();
        long testTime = System.nanoTime();
        TimestampHandler.startProcessing(letter);
        assertTrue(letter.getStartProcessingTimestamp() > testTime);
    }

    /**
     * Unit Test zu Aufgabe 11
     */
    @Test
    public void diffLessThan1secStart() {
        Letter letter = new Letter();
        long testTime = System.nanoTime();
        TimestampHandler.startProcessing(letter);
        assertTrue((letter.getStartProcessingTimestamp() - testTime) < SEC_IN_NANOSEC);
    }

    /**
     * Unit Test zu Aufgabe 11
     */
    @Test
    public void biggerThan0End() {
        Letter letter = new Letter();
        TimestampHandler.endProcessing(letter);
        assertTrue(letter.getEndProcessingTimestamp() > 0);
    }

    /**
     * Unit Test zu Aufgabe 11
     */
    @Test
    public void biggerThanPreSetVariableEnd() {
        Letter letter = new Letter();
        long testTime = System.nanoTime();
        TimestampHandler.endProcessing(letter);
        assertTrue(letter.getEndProcessingTimestamp() > testTime);
    }

    /**
     * Unit Test zu Aufgabe 11
     */
    @Test
    public void diffLessThan1secEnd() {
        Letter letter = new Letter();
        long testTime = System.nanoTime();
        TimestampHandler.endProcessing(letter);
        assertTrue((letter.getEndProcessingTimestamp() - testTime) < SEC_IN_NANOSEC);
    }
}
