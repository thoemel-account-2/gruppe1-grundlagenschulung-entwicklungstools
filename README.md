# Grundlagenschulung Continuous Integration

## Codeübersicht

    - src
        |- main
            |- java
                |- ch.ergon.edu.ci.mailinator
                    |- distribution
                    |   |- AddressGenerator
                    |   |- ContentGenerator
                    |- domain
                    |   |- AddressEntry
                    |   |- Letter
                    |- validation
                    |   |- LetterValidator
                    |- Mailinator
                  
Die Klassen haben folgende Funktionen:

#### Mailinator
Die Hauptklasse des Programms. Hier wird aufgrund von einer Liste von Adressen in Form von `AddressEntry` eine Liste von Briefen erzeugt. 
Diese Briefe werden durch die beiden Generatoren ``AddressGenerator`` und ``ContentGenerator`` befüllt und durch den ``LetterValidator`` überprüft.
Nur Briefe, die valide sind, werden weiterverarbeitet. 

#### Letter
Diese Klasse repräsentiert einen einzelnen Brief. Er besteht aus verschiedenen Feldern für die Adresse und dem Content.

#### AddressEntry
Das ist der Input mit der Adresse, aufgrund welcher ein Brief erstellt werden soll. 
Die Inhalte aus diesem Entry werden in den Brief in die entsprechenden Felderabgefüllt. 

#### AddressGenerator
Hier findet das Abfüllen der Adresse aus dem ``AddressEntry`` in den ``Letter`` hinein statt.

#### ContentGenerator
Hier wird der Inhalt des Briefes erzeugt.

#### LetterValidator
Mit der Methode ``isValid`` wird überprüft, ob der generierte Brief valide ist.
Die Überprüfung findet in verschiedenen Untermethoden statt.



## Aufgaben

Löst diese Aufgaben in der vorgegebenen Reihenfolge. 
Wenn es möglich ist, teilt die Arbeiten untereinander auf, so dass ihr parallel arbeiten könnt.
Es dürfen höchstens 2 Aufgaben gleichzeitig in Bearbeitung sein.

### Basisaufgaben


#### Aufgabe 1
Entferne in der Klasse ``AddressGeneratorTest`` die Annotation ``@Ignore`` von den Testmethoden, bei denen steht, dass sie sich auf diese Aufgabe beziehen.
Implementiere dann die Methode ``insertAddress`` in der Klasse ``AddressGenerator``, die die Inhalte eines AddressEntry in den Letter abfüllt.

Führe nun die Tests aus. Sie dürfen nicht mehr fehlschlagen. Falls sie doch fehlschlagen, korrigiere deine Implementation der Methode ``insertAddress``,
nicht den Test :-).


#### Aufgabe 2
ContentGenerator: Implementiere die Methode ``insertContent``, die einen zufällige Buchstabenfolge erzeugt und in den Content des Letters einfüllt.

ContentGeneratorTest: Entferne die Annotation ``@Ignore`` von den Testmethoden, bei denen steht, dass sie sich auf diese Aufgabe beziehen.
Für dieses Tests aus. Sie dürfen nicht mehr fehlschlagen. Falls sie doch fehlschlagen, korrigiere deine Implementation der Methode ``insertContent``,
nicht den Test :-).

#### Aufgabe 3
LetterValidator: Implementiere die Validierungsmethode ``isPlzFormatCorrect``. Eine PLZ ist dann korrekt, wenn sie genau 4 Stellen hat. 
Überprüfe deine Implementation, indem du die Tests in der Klasse ``LetterValidatorTest`` aktivierst, die sich auf diese Aufgabe beziehen.  

#### Aufgabe 4
LetterValidator: Implementiere die Validierungsmethode ``isContentNotEmpty``. Der Content ist dann korrekt, wenn er nicht null und nicht der leere String ``""`` ist. 
Überprüfe deine Implementation, indem du die Tests in der Klasse ``LetterValidatorTest`` aktivierst, die sich auf diese Aufgabe beziehen.  

#### Aufgabe 5
LetterValidator: Implementiere die Validierungsmethode ``isValidCity``. Eine Stadt ist dann valide, wenn sie in der Liste der gültigen Städte ``VALID_CITIES`` vorkommt. 
Überprüfe deine Implementation, indem du die Tests in der Klasse ``LetterValidatorTest`` aktivierst, die sich auf diese Aufgabe beziehen.  

#### Aufgabe 6
LetterValidator: Implementiere die Methode ``isValid``, welche die drei privaten Validierungsmethoden aufruft und zurückgibt, dass der Brief valid ist, falls alle Methoden ``true`` zurückgeben.

#### Aufgabe 7
MailinatorTest: Jetzt ist die Funktionalität vollständig und du kannst die Tests in der Klasse ``MailinatorTest`` aktivieren, indem du das ``@Ignore`` entfernst. 
Die Tests sollten erfolgreich durchlaufen.


###Erweiterte Aufgaben

#### Aufgabe 8
AddressGenerator: Die Klasse AddressGenerator hat nur eine Methode darin, die keine Felder der Instanz benötigt. Die Methode könnte also auch statisch sein.
Ändere die Methode so, dass die Methode statisch ist.

#### Aufgabe 9
AddressGenerator: Die Klasse AddressGenerator ist damit eine typische Utility-Klasse, deren Methoden statisch benutzt werden können, die man aber weder instanzieren noch erweitern sollte.
Stell also sicher, dass man keine Subklassen der Klasse machen kann und dass niemand eine Instanz der Klasse erzeugen kann (Tipp: Es hat mit dem Constructor dieser Klasse zu tun).


#### Aufgabe 10
Letter: Erweitere die Klasse Letter um zwei Felder ``startProcessingTimestamp`` und ``endProcessingTimestamp`` vom Typ ``long`` mit den entsprechenden Getter- und Setter-Methoden. 
Kontrolliere die bestehenden Methoden, ob sie angepasst werden müssen.

#### Aufgabe 11
Timestamps befüllen: Erstelle eine Klasse TimestampHandler mit zwei statischen Methoden, welche die beiden Timestamps in der Letter-Klasse setzt.  
Es soll dabei jeweils die aktuelle Systemzeit in Nanosekunden gespeichert werden.
Erstelle auch UnitTests zur Klasse TimestampHandler.

#### Aufgabe 12 
Mailinator: Bau die Funktionalität des TimestampHandlers in den Mailinator ein, so dass auf jedem Brief die Timestamps gesetz werden.
Der erste Timestamp soll direkt nach der Erstellung der Instanz geschrieben werden, der zweite nach Abschluss der Validierung.

#### Aufgabe 13
Letter: Erweitere die Klasse Letter um ein Feld, in welchem die Signatur des Absenders gespeichert werden kann.

#### Aufgabe 14
SignatureGenerator: Erstelle eine Klasse ``SignatureGenerator``, mit welcher die Signatur in den Brief abgefüllt wird. Die Signatur, welche verwendet wird, wird aus einem File gelesen.
Schreibe einen UnitTest dazu.

#### Aufgabe 15
Mailinator: Bau die Funktionalität des SignatureGenerators in den Mailinator ein, so dass jeder Brief die Signatur erhält.


 
 

